import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { CoreDataService } from '../core-data.service';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { OrderBookComponent } from '../order-book/order-book.component';
import { TradesComponent } from '../trades/trades.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: any;
  password: any;
  userId: any;
  error: boolean;
  accessToken: any;
  refreshToken: any;
  expiresIn: any;
  logoutReason: any;
  loader: boolean;
  currentRoute;
  otpBlock: boolean = false;
  otp: any;

  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private cookie: CookieService,
    private route: Router,
    private nav: NavbarComponent,
    private orderBook: OrderBookComponent,
    private trade: TradesComponent) { }

  ngOnInit() {
    this.error = false;
    this.loader = false;
    this.logoutReason = this.nav.reason;
    this.route.events.subscribe(val => {
      this.currentRoute = val;
      if (this.currentRoute.url != '/dashboard' && this.orderBook.source12) {
        this.orderBook.source12.close();
        this.orderBook.source2.close();
        //this.trade.getMarketTradeBuy.close();
      }
    });
  }

  basic() {
    //location.href = 'https://www.paybito.com/dashboard/login';
  }
  loginData(isValid: boolean) {

    if (isValid && this.email !="" &&this.password !="") {
      this.loader = true;
      this.logoutReason = '';
      var loginObj = {};
      loginObj['email'] = this.email;
      loginObj['password'] = this.password;
      var jsonString = JSON.stringify(loginObj);
      //login webservice
      this.http.post<any>(this.data.WEBSERVICE + '/user/LoginWithUsernamePassword', jsonString, { headers: { 'content-Type': 'application/json' } })
        .subscribe(data => {
          // error
          if (data.error.error_data == '1') {
            this.error = true;
            this.loader = false;
          }
          // valid
          if (data.error.error_data == '0') {
            this.error = false;

            if (data.userResult.twoFactorAuth == 0) {
              this.setLoginData(data);
            }
            else {
              this.loader = false;
              this.otpBlock = true;
              $('.otp_segment').show();
              $('.otp_btn').show();
              $('.login_btn').hide();
              $('#loginInputOTP').focus();
            }
          }
        }, error => {
          this.loader = true;
        })
    }
    else{
      this.error = true;
    }

  }

  loginThroughOtp() {
    var otpObj = {};
    otpObj['email'] = this.email;
    otpObj['otp'] = this.otp;
    var jsonString = JSON.stringify(otpObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/user/CheckTwoFactor', jsonString, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .subscribe(response => {
        // wip(0);
        var result = response;
        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.setLoginData(result);
        }
      },
        reason => {
          this.data.alert(reason, 'danger')
        });
  }

setLoginData(data){
  this.userId = data.userResult.userId;
  let body = new URLSearchParams();
  body.set('username', this.userId);
  body.set('password', this.password);
  body.set('grant_type', 'password');
  let options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('authorization', 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg==')
  };
  this.http.post<any>(this.data.WEBSERVICE + '/oauth/token', body.toString(), options)
    .subscribe(dataAuth => {
      this.accessToken = dataAuth.access_token;
      this.refreshToken = dataAuth.refresh_token;
      this.expiresIn = dataAuth.expires_in;
      localStorage.setItem('access_token', this.accessToken);
      localStorage.setItem('refresh_token', this.refreshToken);
    // console.log(this.accessToken,'++++++++',this.refreshToken);
      var expiresTime = this.expiresIn;
      expiresTime = expiresTime * 300000;
      var start_time = $.now();
      var expiresIn = start_time + expiresTime;
      localStorage.setItem('expires_in', expiresIn);

      var userObj={};
      userObj['userId']=this.userId;
      var userJsonString=JSON.stringify(userObj);

      this.http.post<any>(this.data.WEBSERVICE+'/user/GetUserDetails',userJsonString,{headers: {'Content-Type': 'application/json','authorization': 'BEARER '+this.accessToken}})
      .subscribe(dataRecheck=>{
        //console.log('dataRecheck+++++',dataRecheck.userListResult[0].fullName);
        if(dataRecheck.error.error_data=='0'){
          localStorage.setItem('user_name',dataRecheck.userListResult[0].fullName);
          localStorage.setItem('user_id',dataRecheck.userListResult[0].userId);
          localStorage.setItem('phone',dataRecheck.userListResult[0].phone);
          localStorage.setItem('email',dataRecheck.userListResult[0].email);
          localStorage.setItem('address',dataRecheck.userListResult[0].address);
          localStorage.setItem('profile_pic',dataRecheck.userListResult[0].profilePic);
          localStorage.setItem('check_id_verification_status','true');
          localStorage.setItem('selected_currency','eth');
          localStorage.setItem('buying_crypto_asset','eth');
          localStorage.setItem('selling_crypto_asset','btc');
          this.cookie.set('access_token',localStorage.getItem('access_token'),60);
          this.route.navigateByUrl('/dashboard');
          this.data.alert('Login Successful!','success');
          }
        }, reason => {
                // wip(0);
                this.data.alert(reason, 'danger');
        });
    },reason => {
      // wip(0);
      this.data.alert(reason, 'danger');
    });

}

showHide(){
  $(".showHide-password").each(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
}

}
