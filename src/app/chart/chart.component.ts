import { Component, OnInit, DoCheck } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { OrderBookComponent } from "../order-book/order-book.component";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { TradesComponent } from "../trades/trades.component";
import * as $ from "jquery";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { Action } from "rxjs/internal/scheduler/Action";
import { isNgTemplate } from "@angular/compiler";

@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.css"]
})
export class ChartComponent implements OnInit {
  filePath: string;
  fileDir: string;
  url: any = null;
  chartDataInit: any;
  chartData: any;
  selected: any;
  indicatorStatus: any = 0;
  indicatorName: any = "";
  apiUrl: string;
  apiData: any;
  loader: boolean;
  errorText: string;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  currencyListEur: any = 0;
  currencyListBtc: any = 0;
  currencyListEth: any = 0;
  currencyListUsd: any = 0;
  buyPriceText: any;
  chartD: any = "d";
  dateD: any = "1m";
  indicatorGroup1: any;
  indicatorGroup2: any;
  highValue;
  lowValue;
  cur: any;
  tools: boolean;
  chartlist: any;
  ctpdata: any = 0;
  lowprice: any = 0;
  highprice: any = 0;
  rocdata: number = 0;
  action: any;
  volumndata: any = 0;
  act: any;
  react: boolean;
  buyingAssetIssuer: string;
  sellingAssetIssuer: string;
  //rocact:any;
  rocreact: boolean;
  droc: any;
  negetive: boolean;
  Tonight: boolean;
  Tomorning: boolean;
  filterCurrency: any;
  logic: boolean;
  flag: boolean;
  Cname: any;
  testval = [];
  currency_code: any;
  base_currency: any;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  header: any;
  assets: any;
  currencyId: any;
  constructor(
    private http: HttpClient,
    public data: CoreDataService,
    private orderBook: OrderBookComponent,
    private stoploss: StopLossComponent,
    private trade: TradesComponent,
    public dash: DashboardComponent
  ) { }

  ngOnInit() {
    this.currency_code = 'ETH';
    this.base_currency = 'BTC';
    localStorage.setItem("buying_crypto_asset",'ETH');
    localStorage.setItem("selling_crypto_asset",'BTC');
    localStorage.setItem("CurrencyID",'3')
    $(".selected").removeClass("active");
    $("#" + "1m").addClass("active");

    $(".bg_new_class")
      .removeClass("bg-dark")
      .css("background-color", "#16181a");
    this.indicatorGroup1 = this.data.indicatorGroup1;
    this.indicatorGroup2 = this.data.indicatorGroup2;

    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName = this.base_currency + this.currency_code;

    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText = this.currency_code;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText = this.base_currency;
    this.selectedCryptoCurrencySymbol = this.data.selectedCryptoCurrencySymbol =
      "./assets/img/currency/" +
      this.data.selectedSellingAssetText.toUpperCase() +
      ".svg";

    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer = "GDZN5MUTRBPV3INIZNOZX2HTRMH4LZDQBWPBUWNTHI3GGNKW56DP5GSY";
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer = "GAWLWFKLVQAOWVJJE5I5ADLW44AYEUCPCD7AT3Q73APS22HILWLSFEGX";
    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth();
    var dt = d.getDate();
    var prdt = y + "-" + m + "-" + dt;
    var nxdt = y + "-" + (m + 1) + "-" + dt;
    this.apiData = {
      from_date: prdt.toString(),
      to_date: nxdt.toString(),
      chart_duration: "m",
      currency: this.data.selectedBuyingAssetText.toLocaleLowerCase(),
      base_currency: this.data.selectedSellingAssetText.toLocaleLowerCase()
    };
    this.fileDir = "./assets/data/";
    this.filePath =
      this.fileDir +
      this.data.selectedBuyingAssetText.toLocaleLowerCase() +
      this.data.selectedSellingAssetText.toLocaleLowerCase() +
      ".json";
    this.graphData(0, "", this.chartD, this.dateD);
    this.orderBook.tradePageSetup();
    $("#" + this.dateD).addClass("active");
    //  this.stoploss.getUserTransaction();
    $(".reset").click();
    this.tools = true;
    $("#dropHolder").css("overflow", "scroll");
    $(window).resize(function () {
      var wd = $("#chartHolder").width();
      $("#dropHolder").width(wd);
      $("#dropHolder").css("overflow", "scroll");
    });
    this.http
      .get<any>(
        this.data.CHARTSERVISE +
        "trendsTradeGraphFor24Hours/" +
        this.currency_code +
        "/" +
        this.base_currency
      )
      .subscribe(value => {
        if (value != "") {
          this.chartlist = value[0];
          this.ctpdata = this.chartlist.ctp;
          this.lowprice = this.chartlist.low_price;
          this.highprice = this.chartlist.high_price;
          this.act = this.chartlist.action;
          this.rocdata = this.chartlist.roc.toFixed(2);
          if (this.rocdata > 0) {
            //  this.negetive =
            this.rocreact = true;
            //alert ("Hi");//this.rocreact = true}
          } else {
            //console.log(this.rocreact);
            this.rocreact = false;
          }
          if (this.rocdata < 0) {
            this.negetive = true;
          } else {
            this.negetive = false;
          }
          //this.droc = this.rocdata.
          this.volumndata = this.chartlist.volume.toFixed(2);

          //  if (this.rocdata >= 0){this.rocreact = true}
          //this.droc = this.rocdata.

          if (this.rocdata >= 0) {
            this.rocreact = true;
          }

          if (this.act == "sell") {
            this.react = true;
          }
        } else {
          this.ctpdata = 0;
          this.lowprice = 0;
          this.highprice = 0;
          this.rocdata = 0;
          this.volumndata = 0;
          // alert(123);
          // console.log(123);
        }
      });
    //this.changemode();
  }

  changemode() {
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "#dedede");
      $(".btn").css("border-color", "#b3b4b4");
      $(".btn").css("color", "#000");
      $(".charts-tab.active").css("background-color", "#fefefe");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".btn")
        .removeClass("bg-black")
        .css("background-color", "rgb(44, 65, 66)");
      $(".btn").css("border-color", "transparent");
      $(".btn").css("color", "#fff");
      $(".charts-tab.active").css("background-color", "#242e3e");
    }
  }
  ngDoCheck() {
    this.changemode();
  }

  buySellCode(item) {

    //   (item.currency=="BCC")?this.currency_code ="BCH":this.currency_code = item.currency; //changed by sanu
    this.currency_code = item.currencyCode;
    this.base_currency = item.baseCurrency;
    this.currencyId = item.currencyId;//change by sanu
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingAssetText = item.currencyCode;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    localStorage.setItem("CurrencyID", this.currencyId);//change bu sanu
    this.assetpairsell = item.currencyCode + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + item.currencyCode;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/GetAssetIssuer')
      .subscribe(responseBuySell => {
        //   console.log('responseBuySell+++++++++++', responseBuySell);
        this.responseBuySell = responseBuySell;
        var x = this.responseBuySell.length;
        for (var i = 0; i < x ; i++) {
          if (this.assetpairsell == this.responseBuySell[i].assetPair) {
            this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[i].issuer;

          }
          else if (this.assetpairbuy == this.responseBuySell[i].assetPair) {
            this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[i].issuer;
          }
          else if (this.data.selectedSellingCryptoCurrencyissuer != "" && this.data.selectedBuyingCryptoCurrencyissuer != "") {
            break;
          }
        }

        var d = new Date;
        var y = d.getFullYear();
        var m = d.getMonth();
        var dt = d.getDate();
        var prdt = y + '-' + m + '-' + dt;
        var nxdt = y + '-' + (m + 1) + '-' + dt;
        this.apiData = {
          "from_date": prdt.toString(),
          "to_date": nxdt.toString(),
          "chart_duration": "m",
          "currency": this.currency_code.toLocaleLowerCase(),
          "base_currency": this.base_currency.toLocaleLowerCase()
        };
       // console.log('base_currency+++++++++', this.base_currency.toLocaleLowerCase());
        this.fileDir = './assets/data/';
        this.filePath = this.fileDir + this.currency_code.toLocaleLowerCase() + this.base_currency.toLocaleLowerCase() + '.json';
       // console.log('base_currency+++++++++', this.base_currency);

        this.graphData(0, '', this.chartD, this.dateD);

        this.orderBook.tradePageSetup();
        // this.orderBook.serverSentEventForOrderbookAsk();
        // this.orderBook.serverSentEventForOrderbookBid();
        this.trade.reload();
        this.trade.getMarketTradeBuy();
        this.trade.myTradeDisplay(0);
        $('#' + this.dateD).addClass('active');
        this.stoploss.getUserTransaction();
        $('.reset').click();
        // this.url = './assets/tradingview/index.html?cur=BTCUSD';
        this.tools = true;
        $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
        this.http.get<any>(this.data.CHARTSERVISE + '/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency)
          .subscribe(value => {
            if (value != '') {
              this.chartlist = value[0];
              this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
              this.lowprice = this.data.lowprice = this.chartlist.low_price;
              this.highprice = this.data.highprice = this.chartlist.high_price;
              this.act = this.chartlist.action;
              // console.log('ctpdata_______________', this.act, this.ctpdata);
              this.rocdata = this.chartlist.roc.toFixed(2);
              if (this.rocdata > 0) {
                //  this.negetive =
                this.rocreact = true;
                //alert ("Hi");//this.rocreact = true}
              }
              else {
                this.rocreact = false;
              }
              if (this.rocdata < 0) {
                this.negetive = true;
              }
              else {
                this.negetive = false;
              }
              //this.droc = this.rocdata.
              this.volumndata = this.chartlist.volume.toFixed(2);
              //  if (this.rocdata >= 0){this.rocreact = true}
              //this.droc = this.rocdata.

              if (this.rocdata >= 0) { this.rocreact = true }

              if (this.act == 'sell') {

                this.react = true;
              }
              else {
                this.react = false;
              }
            } else {
              this.ctpdata = 0;
              this.lowprice = 0;
              this.highprice = 0;
              this.rocdata = 0;
              this.volumndata = 0;
              // alert(123);
              // console.log(123);

            }
          })

      })
  }

  getNewCurrency() {
    this.http.get<any>(this.data.WEBSERVICE + '/userTrade/GetCurrencyDetails')
      .subscribe(responseCurrency => {
        this.filterCurrency = responseCurrency;
        var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;
      }
      )
  }
  // graph data
  graphData(
    indicatorStatus: any,
    indicatorName: any,
    chartDuration: any,
    dateDuration: any): void {
    this.indicatorStatus = indicatorStatus;
    this.indicatorName = indicatorName;
    this.selected = indicatorName;
    this.chartD = chartDuration;
    this.dateD = dateDuration;
    this.loader = true;
    this.errorText = null;
    $(".selected").removeClass("active");
    $("#" + dateDuration).addClass("active");
    //time
    //Current Date
    var currentDates = new Date();
    var currentYear = currentDates.getFullYear();
    var currentMonth = currentDates.getMonth() + 1;
    // alert(currentMonth);
    var currentDate = currentDates.getDate();
    var currentDateInput = currentYear + "-" + currentMonth + "-" + currentDate;

    //Three Months Previous Date
    var threeMonthsPreviousDateUnix = currentDates.setFullYear(
      currentDates.getFullYear(),
      currentMonth - 3
    );
    var threeMonthsPreviousDate = new Date(threeMonthsPreviousDateUnix);
    if (threeMonthsPreviousDate.getMonth() == 0) {
      var threeMonthsPreviousDateInput =
        threeMonthsPreviousDate.getFullYear() +
        "-12-" +
        threeMonthsPreviousDate.getDate();
    } else {
      var threeMonthsPreviousDateInput =
        threeMonthsPreviousDate.getFullYear() +
        "-" +
        threeMonthsPreviousDate.getMonth() +
        "-" +
        threeMonthsPreviousDate.getDate();
    }

    //Six Months Previous Date
    var sixMonthsPreviousDateUnix = currentDates.setFullYear(
      currentYear,
      currentMonth - 6
    );
    var sixMonthsPreviousDate = new Date(sixMonthsPreviousDateUnix);
    if (sixMonthsPreviousDate.getMonth() == 0) {
      var sixMonthsPreviousDateInput =
        sixMonthsPreviousDate.getFullYear() -
        1 +
        "-12-" +
        sixMonthsPreviousDate.getDate();
    } else {
      var sixMonthsPreviousDateInput =
        sixMonthsPreviousDate.getFullYear() +
        "-" +
        sixMonthsPreviousDate.getMonth() +
        "-" +
        sixMonthsPreviousDate.getDate();
    }

    //Year Previous Date
    var twlMonthsPreviousDateUnix = currentDates.setFullYear(
      currentYear - 1,
      currentMonth
    );
    var twlMonthsPreviousDate = new Date(twlMonthsPreviousDateUnix);
    var twlMonthsPreviousDateInput =
      twlMonthsPreviousDate.getFullYear() +
      "-" +
      (twlMonthsPreviousDate.getMonth() + 1) +
      "-" +
      twlMonthsPreviousDate.getDate();

    //last Week Date
    var myDate = new Date();
    var newDate = new Date(myDate.getTime() - 60 * 60 * 24 * 7 * 1000);
    var lastWeekDate =
      newDate.getFullYear() +
      "-" +
      (newDate.getMonth() + 1) +
      "-" +
      newDate.getDate();

    //last week date
    var yesterdayDateUnix = new Date(Date.now() - 864e5);
    var yesterdayDate =
      yesterdayDateUnix.getFullYear() +
      "-" +
      (yesterdayDateUnix.getMonth() + 1) +
      "-" +
      yesterdayDateUnix.getDate();

    //1 month date
    var lastMonthDate = new Date(currentYear, currentMonth - 2, currentDate);
    var lastMonthDateForInput =
      lastMonthDate.getFullYear() +
      "-" +
      (lastMonthDate.getMonth() + 1) +
      "-" +
      lastMonthDate.getDate();

    var inputObj = {};
    if (dateDuration != undefined) {
      if (dateDuration == "3m") {
        var fromDate = threeMonthsPreviousDateInput;
      }
      if (dateDuration == "6m") {
        var fromDate = sixMonthsPreviousDateInput;
      }
      if (dateDuration == "12m") {
        var fromDate = twlMonthsPreviousDateInput;
      }
      if (dateDuration == "1w") {
        var fromDate = lastWeekDate;
      }
      if (dateDuration == "1d") {
        var fromDate = yesterdayDate;
      }
      if (dateDuration == "1m") {
        var fromDate = lastMonthDateForInput;
      }
    } else {
      var fromDate = lastMonthDateForInput;
    }

    inputObj["fromDate"] = fromDate;
    inputObj["toDate"] = currentDateInput;
    if (chartDuration == undefined) {
      inputObj["chartDuration"] = "d";
    } else {
      inputObj["chartDuration"] = chartDuration;
    }
    inputObj["currency"] = this.currency_code.toLocaleLowerCase();
    inputObj["baseCurrency"] = this.base_currency.toLocaleLowerCase();
    var jsonString = JSON.stringify(inputObj);
    // console.log(jsonString);
    //time
    localStorage.setItem("dateDuration", dateDuration);
    // api call//////////////////////////////////////
    this.apiUrl = this.data.WEBSERVICE + "/userTrade/GetTradeChart";
    this.apiData = jsonString;
    this.http
      .post<any>(this.apiUrl, this.apiData, {
        headers: { "content-Type": "application/json" }
      })
      .subscribe(
        data => {
          // console.log('_____++++++++',data);
          this.chartDataInit = data.apiResponse;
          var apiResponse = JSON.parse(this.chartDataInit);

          var graphDataArr = [];
          if (apiResponse.length) {
            for (var i = 0; i < apiResponse.length; i++) {
              var resultArrM = [];

              if (parseFloat(apiResponse[i].volume) != 0.0) {
                var dateArr = [];
                dateArr = apiResponse[i].timeStamp;
                resultArrM.push(dateArr);
                resultArrM.push(parseFloat(apiResponse[i].openPrice));
                resultArrM.push(parseFloat(apiResponse[i].highPrice));
                resultArrM.push(parseFloat(apiResponse[i].lowPrice));
                resultArrM.push(parseFloat(apiResponse[i].closePrice));
                resultArrM.push(parseFloat(apiResponse[i].volume));
                graphDataArr.push(resultArrM);
              }
            }
            var buyPrice: any = parseFloat(
              apiResponse[apiResponse.length - 1].openPrice
            );
          }
          if (buyPrice) buyPrice = buyPrice > 0 ? buyPrice : "0" + buyPrice;
          else buyPrice = 0;
          this.cur =
            this.data.selectedBuyingAssetText === "USD"
              ? "$"
              : this.data.selectedSellingAssetText + " ";
          this.buyPriceText = buyPrice;

          this.http
            .get(this.filePath, {
              headers: {
                "content-Type": "application/json"
              }
            })
            .subscribe(
              subdata => {
                if (subdata) {
                  var apiResponse: any = subdata;

                  for (var i = 0; i < apiResponse.length; i++) {
                    if (parseFloat(apiResponse[i].volume) != 0.0) {
                      var resultArr = [];
                      var dateArr = [];
                      dateArr = apiResponse[i].Date;
                      resultArr.push(dateArr);
                      resultArr.push(parseFloat(apiResponse[i].Open));
                      resultArr.push(parseFloat(apiResponse[i].High));
                      resultArr.push(parseFloat(apiResponse[i].Low));
                      resultArr.push(parseFloat(apiResponse[i].Price));
                      resultArr.push(parseFloat(apiResponse[i].Vol));
                      graphDataArr.push(resultArr);
                    }
                    ``;
                  }
                }

                //  data call
                this.chartData = JSON.stringify(graphDataArr);
                localStorage.setItem("chartData", this.chartData);
                this.url =
                  "./assets/chart.html?name=" +
                  this.currency_code +
                  "&indicatorStatus=" +
                  this.indicatorStatus +
                  "&indicatorName=" +
                  this.indicatorName +
                  "&buy=" +
                  this.selectedBuyingCryptoCurrencyName +
                  "&sell=" +
                  this.selectedSellingCryptoCurrencyName +
                  "&fromDate=" +
                  fromDate;
                // this.url = './assets/tradingview/index.html?cur='+this.selectedBuyingCryptoCurrencyName.toUpperCase()+this.selectedSellingCryptoCurrencyName.toUpperCase();
                this.loader = false;
                if (this.orderBook.source12) {
                  this.orderBook.source12.close();
                   this.orderBook.source2.close();
                }
                // this.orderBook.tradePageSetup();
                 this.stoploss.getUserTransaction();
              },
              error => {
                this.loader = false;
                this.errorText = error.statusText;
                this.url = null;
              }
            ); //get subscribe
        },
        error => { }
      ); //post subscribe
  }

  //clear indicators
  destroy(): void {
    this.selected = null;
    this.graphData(0, "", this.chartD, this.dateD);
  }

}
